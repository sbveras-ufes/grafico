/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pss.grafico.model;

import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author UFES
 */
public class Dataset {
   
    public static DefaultCategoryDataset createDataset() {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    String series1 = "Homem";
    String series2 = "Mulher";
    
    dataset.addValue(5.0, series1, "Solteirx");
    dataset.addValue(4.8, series1, "Casadx");
    
    dataset.addValue(4.0, series2, "Solteirx");
    dataset.addValue(4.2, series2, "Casadx");
//    dataset.addValue(3.8, series2, "2007");
//    dataset.addValue(3.6, series2, "2008");
//    dataset.addValue(3.4, series2, "2009");
//    dataset.addValue(3.4, series2, "2010");
//    dataset.addValue(3.3, series2, "2011");
//    dataset.addValue(3.1, series2, "2012");
//    dataset.addValue(3.2, series2, "2013");
// 
//    dataset.addValue(3.6, series3, "2005");
//    dataset.addValue(3.4, series3, "2006");
//    dataset.addValue(3.5, series3, "2007");
//    dataset.addValue(3.2, series3, "2008");
//    dataset.addValue(3.2, series3, "2009");
//    dataset.addValue(3.0, series3, "2010");
//    dataset.addValue(2.8, series3, "2011");
//    dataset.addValue(2.8, series3, "2012");
//    dataset.addValue(2.6, series3, "2013");
// 
//    dataset.addValue(3.2, series4, "2005");
//    dataset.addValue(3.2, series4, "2006");
//    dataset.addValue(3.0, series4, "2007");
//    dataset.addValue(3.0, series4, "2008");
//    dataset.addValue(2.8, series4, "2009");
//    dataset.addValue(2.7, series4, "2010");
//    dataset.addValue(2.6, series4, "2011");
//    dataset.addValue(2.6, series4, "2012");
//    dataset.addValue(2.4, series4, "2013");
// 
    return dataset;
    
} 
}
