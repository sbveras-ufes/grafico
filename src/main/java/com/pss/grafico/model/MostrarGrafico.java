/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pss.grafico.model;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
/**
 *
 * @author UFES
 */
public class MostrarGrafico {
     
private final ChartPanel painel;
private final DefaultCategoryDataset dataset = Dataset.createDataset(); 
private final JFreeChart grafico;
    public  MostrarGrafico(PlotOrientation o) {

	grafico = ChartFactory.createBarChart("", 
		"", 
		"",
		dataset,
		o, 
		true,
		false,
		false);
        (grafico.getLegend()).setVisible(false);
	painel = new ChartPanel(grafico);
}

    public ChartPanel getPainel() {
        return painel;
    }

    public JFreeChart getGrafico() {
        return grafico;
    }
    
}
